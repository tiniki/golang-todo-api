package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"tiniki.ga/todo-api/auth"
	"tiniki.ga/todo-api/todos"
	"tiniki.ga/todo-api/users"
	"tiniki.ga/todo-api/utils"
)

func main() {
	utils.InitEnv()

	utils.InitDB()
	defer utils.Db.Close()
	utils.Db.AutoMigrate(
		&users.User{},
		&todos.Todo{},
	)

	e := echo.New()

	e.Use(
		middleware.BodyLimit("2M"),
		middleware.RemoveTrailingSlash(),
		middleware.Logger(),
		middleware.Gzip(),
		middleware.Secure(),
	)

	e.Validator = utils.NewValidator()

	users.Register(e.Group("/users"))
	auth.Register(e.Group("/auth"))
	todos.Register(e.Group("/todos"))

	e.Debug = true
	e.Start(":1323")
}
