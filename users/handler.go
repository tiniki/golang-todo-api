package users

import (
	"net/http"

	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
	"tiniki.ga/todo-api/utils"
)

func HashPassword(password string) (hash []byte, err error) {
	return bcrypt.GenerateFromPassword([]byte(password), 10)
}

func CreateUser(c echo.Context) (err error) {
	u := new(User)
	if err = c.Bind(u); err != nil {
		return
	}

	var hash []byte
	if hash, err = HashPassword(u.Password); err != nil {
		return
	}
	u.Password = string(hash)

	if err = utils.Db.Create(&u).Error; err != nil {
		return
	}
	return c.JSON(http.StatusOK, u)
}

func GetUser(c echo.Context) (err error) {
	var u User
	if err = utils.Db.First(&u, c.Param("id")).Error; err != nil {
		return
	}
	return c.JSON(http.StatusOK, u)
}

func UpdateUser(c echo.Context) (err error) {
	newUser := new(User)
	if err = c.Bind(newUser); err != nil {
		return
	}

	var u User
	if err = utils.Db.First(&u, c.Param("id")).Error; err != nil {
		return
	}

	if newUser.Password != "" {
		var hash []byte
		if hash, err = HashPassword(newUser.Password); err != nil {
			return
		}
		newUser.Password = string(hash)
	}

	if err = utils.Db.Model(&u).Updates(newUser).Error; err != nil {
		return
	}

	return c.JSON(http.StatusOK, &u)
}

func DeleteUser(c echo.Context) (err error) {
	var u User
	if err = utils.Db.First(&u, c.Param("id")).Error; err != nil {
		return
	}

	if err = utils.Db.Delete(&u).Error; err != nil {
		return
	}

	return c.JSON(http.StatusOK, &u)
}
