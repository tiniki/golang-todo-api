package users

import (
	"github.com/jinzhu/gorm"
	"tiniki.ga/todo-api/todos"
)

type User struct {
	gorm.Model
	Name     string       `json:"name" form:"name"`
	Email    string       `json:"email" form:"email" gorm:"type:varchar(100);unique_index"`
	Password string       `json:"password" form:"password"`
	Todos    []todos.Todo `gorm:"foreignkey:UserID"`
}
