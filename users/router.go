package users

import (
	"github.com/labstack/echo"
	"tiniki.ga/todo-api/utils"
)

func Register(g *echo.Group) {
	g.Use(utils.JwtMw)
	g.GET("/:id", GetUser)
	g.POST("", CreateUser)
	g.PUT("/:id", UpdateUser)
	g.DELETE("/:id", DeleteUser)
}
