package utils

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
	"time"
)

var jwtSecret = []byte(os.Getenv("JWT_SECRET"))
var JwtMw = middleware.JWT(jwtSecret)

func JwtSign(id uint) (t string, err error) {
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = id
	claims["exp"] = time.Now().AddDate(0, 0, 7).Unix()

	// Generate encoded token and send it as response.
	return token.SignedString(jwtSecret)
}

func JwtGetId(c echo.Context) uint {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	return uint(claims["id"].(float64))
}
