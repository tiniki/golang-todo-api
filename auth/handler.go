package auth

import (
	"net/http"

	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
	"tiniki.ga/todo-api/users"
	"tiniki.ga/todo-api/utils"
)

func Login(c echo.Context) (err error) {
	params := new(LoginRequestParams)
	if err = c.Bind(params); err != nil {
		return
	}

	var u users.User
	if err = utils.Db.First(&u, users.User{Email: params.Email}).Error; err != nil {
		return
	}

	if err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(params.Password)); err != nil {
		return
	}

	var t string
	if t, err = utils.JwtSign(u.ID); err != nil {
		return
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func Signup(c echo.Context) (err error) {
	u := new(users.User)
	if err = c.Bind(u); err != nil {
		return
	}

	var hash []byte
	if hash, err = users.HashPassword(u.Password); err != nil {
		return
	}
	u.Password = string(hash)

	if err = utils.Db.Create(&u).Error; err != nil {
		return
	}

	var t string
	if t, err = utils.JwtSign(u.ID); err != nil {
		return
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}
