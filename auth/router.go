package auth

import (
	"github.com/labstack/echo"
)

func Register(g *echo.Group) {
	g.POST("/login", Login)
	g.POST("/signup", Signup)
}
