package todos

import (
	"github.com/jinzhu/gorm"
)

type Todo struct {
	gorm.Model
	Title  string `json:"title" form:"title"`
	Done   bool   `json:"done" form:"done"`
	UserID uint
}
