package todos

import (
	"github.com/labstack/echo"
	"tiniki.ga/todo-api/utils"
)

func Register(g *echo.Group) {
	g.Use(utils.JwtMw)
	g.GET("/:id", GetTodo)
	g.POST("", CreateTodo)
	g.PUT("/:id", UpdateTodo)
	g.DELETE("/:id", DeleteTodo)
	g.GET("", GetTodos)
}
