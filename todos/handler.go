package todos

import (
	"net/http"

	"github.com/labstack/echo"
	"tiniki.ga/todo-api/utils"
)

func CreateTodo(c echo.Context) (err error) {
	todo := new(Todo)
	if err = c.Bind(todo); err != nil {
		return
	}

	todo.UserID = utils.JwtGetId(c)

	if err = utils.Db.Create(&todo).Error; err != nil {
		return
	}
	return c.JSON(http.StatusCreated, todo)
}

func GetTodo(c echo.Context) (err error) {
	var todo Todo
	if err = utils.Db.First(&todo, c.Param("id")).Error; err != nil {
		return
	}
	return c.JSON(http.StatusOK, todo)
}

func UpdateTodo(c echo.Context) (err error) {
	newTodo := new(Todo)
	if err = c.Bind(newTodo); err != nil {
		return
	}

	var todo Todo
	if err = utils.Db.First(&todo, c.Param("id")).Error; err != nil {
		return
	}

	if err = utils.Db.Model(&todo).Updates(newTodo).Error; err != nil {
		return
	}

	return c.JSON(http.StatusOK, &todo)
}

func DeleteTodo(c echo.Context) (err error) {
	var todo Todo
	if err = utils.Db.First(&todo, c.Param("id")).Error; err != nil {
		return
	}

	if err = utils.Db.Delete(&todo).Error; err != nil {
		return
	}

	return c.JSON(http.StatusOK, &todo)
}

func GetTodos(c echo.Context) (err error) {
	var todos []Todo
	if err = utils.Db.Find(&todos, &Todo{UserID: utils.JwtGetId(c)}).Error; err != nil {
		return
	}

	return c.JSON(http.StatusOK, &todos)
}
